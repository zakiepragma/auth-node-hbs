const mysql = require("mysql");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const uuid = require("uuid");

const db = mysql.createConnection({
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE,
});

exports.register = (req, res) => {
  console.log(req.body);

  const { name, email, password, confirmPassword, role } = req.body;

  const now = new Date(); // Waktu saat ini
  // Generate UUID untuk pengguna
  const generate_uuid = uuid.v4();

  db.query(
    "SELECT email FROM users WHERE email = ?",
    [email],
    async (error, results) => {
      if (error) {
        console.log(error);
      }

      if (results.length > 0) {
        return res.render("register", {
          message: "This email is already in use",
        });
      } else if (password !== confirmPassword) {
        return res.render("register", {
          message: "Password do not match",
        });
      }

      let hashPassword = await bcrypt.hash(password, 8);

      console.log(hashPassword);

      db.query(
        "INSERT INTO users SET ?",
        {
          uuid: generate_uuid,
          name: name,
          email: email,
          password: hashPassword,
          role: role,
          createdAt: now,
          updatedAt: now,
        },
        (error, results) => {
          if (error) {
            console.log(error);
          } else {
            console.log(results);
            return res.render("register", {
              message: "User registered",
            });
          }
        }
      );
    }
  );

  // res.send("form submitedd");
};
